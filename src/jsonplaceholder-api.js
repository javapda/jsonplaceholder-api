const axios = require('axios')
const _ = require('lodash')

module.exports = class JsonPlaceholder {
    constructor() {
        this.serverUrl='https://jsonplaceholder.typicode.com'
    }

    async posts() {
        var args = Array.prototype.slice.call(arguments);
        const props = {}; //{params:{fields:args.join(";")}}
        const url = `${this.serverUrl}/posts`
        return (await axios(url,props)).data
    }


}